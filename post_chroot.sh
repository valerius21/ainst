#!/usr/bin/env bash

ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc

echo en_US.UTF-8 UTF-8 >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf

echo "change_me" > /etc/hostname

mkinitcpio -p linux
passwd

pacman -S grub vim zsh mpv git curl wget linux-headers

grub-install /dev/sda
echo reboot manually!
exit
