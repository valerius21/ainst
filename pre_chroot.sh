#!/usr/bin/env bash

# base install
timedatectl set-ntp true
cfdisk
mkfs.vfat -f -F32 /dev/sda1
mkfs.xfs -f /dev/sda3
mkswap /dev/sda2
swapon /dev/sda2
mount /dev/sda3 /mnt
mkdir -p /mnt/boot
mount /dev/sda1 /mnt/boot
pacstrap -i /mnt base base-devel net-tools --noconfirm
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt
